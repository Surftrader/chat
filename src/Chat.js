import React from 'react';
import Header from './components/Header';
import MessageList from './components/MessageList';
import MessageInput from './components/MessageInput';
// import Message from './components/MessageList';
// import OwnMessage from './components/OwnMessage';
// import Preloader from './components/Preloader';
// import chat from './css/chat.css';

class Chat extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      url: 'string',
      title: 'chat name',
      countUser: 0,
      countMessages: 0,
      loading: true,
      messages: [],
      lastMessageDate: null
    }
  }

  async componentDidMount() {
    const response = await fetch(this.props.url);
    const data = await response.json();
    this.setState({
      messages: data,
      loading: false,
      countMessages: data.length,
      lastMessageDate: data[data.length - 1].createdAt
    });
  }

  render() {

    function normalizeDate(value) {
      return new Date(value).toLocaleDateString('ru-Uk', { hour: 'numeric', minute: 'numeric' }).replace(',', '');
    }

    function countUsers(data) {
      const result = data.map(function (message) {
        return message.userId;
      });
      return new Set(result).size;
    }

    return (
      <div className="chat" >
        <Header
          title={this.state.title}
          countUser={countUsers(this.state.messages)}
          countMessages={this.state.countMessages}
          lastMessageDate={normalizeDate(this.state.lastMessageDate)}
        />
        <MessageList
          loading={this.state.loading}
          messages={this.state.messages}
        />
        <MessageInput />

        {/* <div className="row"><OwnMessage /></div> */}
        {/* <div className="row"><Preloader /></div> */}
      </div>
    );
  }
}

export default Chat;
