import React from 'react';
import message from '../css/message.css';

class Message extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        const data = this.props;
        function normalizeTime(date) {
            return new Date(date).toLocaleTimeString('ru-Uk', { hour: 'numeric', minute: 'numeric' });
        }
        return (
            <div className="card-message ">
                <div className="container">
                    <div className="image-wrapper"><img src={data.avatar} alt="Avatar" />
                        <div className="middle">
                            <div className="message-user-name">{data.user}</div>
                        </div>
                    </div>

                </div>
                <div className="message-text">{data.text}</div>
                <div className="message-time">({normalizeTime(data.time)})</div>
                {/* <div className="message-like">{data.like}</div> */}
            </div>

        )
    }
}

export default Message;
