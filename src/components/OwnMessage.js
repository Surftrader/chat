import React from 'react';

class OwnMessage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: 'own text',
            time: '15:35'
        }
    }

    render() {
        const data = this.state;
        return (
            <div>
                <div className="message-text">{data.text}</div>
                <div className="message-time">{data.time}</div>
                <button className="edit">Edit</button>
                <button className="delete">Delete</button>
            </div>
        )
    }
}

export default OwnMessage;
