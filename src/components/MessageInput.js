import React from 'react';

class MessageInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: 'input text'
        }
    }


    render() {
        const data = this.state;
        return (
            <div>
                <input type="text" className="message-input-text" value={data.text} />
                <button type="submit" className="message-input-button">Send</button>
            </div>
        )
    }
}

export default MessageInput;
