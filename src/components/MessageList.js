import React from 'react';
import messageList from '../css/messageList.css';
import Message from './Message';

class MessageList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        const data = this.props;
        if (data.loading) {
            return <div>Loading...</div>
        }

        if (!data.messages.length) {
            return <div>Didn't find any messages</div>
        }

        return (
            <div>
                {data.messages.map(message => (
                    <div key={message.id}>
                        <Message
                            text={message.text}
                            time={message.createdAt}
                            user={message.user}
                            avatar={message.avatar}
                        />

                    </div>
                ))}
            </div>
        );
    }
}

export default MessageList;
