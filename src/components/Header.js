import React from 'react';
import header from '../css/header.css';
import MessageList from './MessageList';

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        const data = this.props;
        return (
            <header>
                <div className="header-title">{data.title}</div>
                <div className="header-users-count">{data.countUser} participants</div>
                <div className="header-messages-count">{data.countMessages} massages</div>
                <div className="header-last-message-date">last message at {data.lastMessageDate}</div>
            </header>
        )
    }
}

export default Header;
